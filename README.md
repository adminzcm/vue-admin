# adminapi开发者后台api管理系统
vue+elementUI+thinkphp
```
作者：yichen 
email：2782268022@qq.com
uni-app 技术交流群：714566447

测试地址:http://cs.01film.cn/aiye/system 注:很多功能被限制。建议下载后用自己服务器测试。
[功能介绍视频](https://mparticle.uc.cn/video.html?uc_param_str=frdnsnpfvecpntnwprdssskt&wm_aid=49e95f6ca0994074b239cb4deff4cbff)
[接口管理-功能介绍视频观看](https://mparticle.uc.cn/video.html?uc_param_str=frdnsnpfvecpntnwprdssskt&wm_aid=cac7ff66a8d94186a3d8207b33908d5d)
[在线预览web](http://cs.01film.cn/qiye/)
## 全部源码下载地址
三个分支:可分离部署web站点。也可以部署到一个站点
请根据tag 版本号下载 大版本号对应即可
如1.0.0——1.0.n都可以 
1站.web前端   https://gitee.com/wokaixin/qiye 
2站.api开发管理web端  https://gitee.com/wokaixin/vue-admin
3站.api接口php服务端  https://gitee.com/wokaixin/php-admin-api

```
[接口管理-功能介绍视频观看](https://mparticle.uc.cn/video.html?uc_param_str=frdnsnpfvecpntnwprdssskt&wm_aid=cac7ff66a8d94186a3d8207b33908d5d)
点击链接加入群聊【thinkphp技术交流群】：https://jq.qq.com/?_wv=1027&k=5KycEAO
## 目录结构【1】 
```
│──public				 入口
│──src					 项目路径
│	│──assets						 静态资源
│	│──common						 公共类库
│	│	│─data				数据目录
│	│	│	│─adminRank.js			用户权限分类
│	│	│	└─menulist.js			菜单分类列表
│	│	│
│	│	│─utils				公共js工具插件目录
│	│	│	└─Message.js		消息弹窗工具
│	│	│─validate			 验证器目录
│	│	│	└─user.js			用户表单验证
│	│	│
│	│	└yc.css				 自定义公共css
│	│──components					公共页面组件
│	│	│──admin						管理团队管理  考虑废除 移植到网站管理后台
│	│	│	│──myfunc.vue				我的权限功能  
│	│	│	│──rank.vue					职务权限管理
│	│	│	│──user.vue				    团队成员管理
│	│	│	└──index.vue				入口
│	│	│──login					 登陆模块目录
│	│	│	│──index						  登陆页面入口文件
│	│	│	│──login						  登陆页面
│	│	│	│──register					  注册页面
│	│	│	└──style.css					 公用css
│	│	│  
│	│	│──public						 公共模块目录
│	│	│	│──chart.vue				后台默认图表页
│	│	│	│──header.vue					页头
│	│	│	│──sidebar.vue				侧边栏
│	│	│	└──tags.vue					标签栏
│	│	│──system						系统管理目录
│	│	│	│──func.vue				    功能管理
│	│	│	│──group.vue				功能组
│	│	│	│──index.vue				入口
│	│	│	│──safe.vue				    安全防御
│	│	│	│──safeUrl.vue				安全拦截url接口限制访问
│	│	│	└──user.vue					管理团队人员
│	│	│
│	│	│──template					  公共模板组件目录
│	│	│	└──echarts.vue			  图表模板
│	│	└──user					    公共模板组件目录
│	│	    │──index.vue				入口
│	│		└──list.vue			        用户管理
│	│──request						ajax服务器请求接口配置目录
│	│──store						  vuex目录
│	│──views						  页面目录
│	│	└──admin						  后台入口目录
│	│		 └──index						  页面入口文件
│	│		 
│	│──App.vue						应用启动加载页面
│	│──config.js					 自定义配置参数
│	│──main.js						应用启动加载js
│	└──router.js					 路由配置
│
└──────────────────────────────────────────────────────────────
```
## Project setup 安装
```
npm install
```
### 安装前请修改配置文件vue.config.js 
```
publicPath节点下的站点目录配置。根据你的站点所在目录进行修改路径
```
### Compiles and hot-reloads for development 本地测试运行
```
npm run serve
```

### Compiles and minifies for production  打包编译
```
npm run build
```
###注意事项
因为部分组件采用cdn引入,npm  build编译发布时需要删除相关引入 npm serve本地运行时候需要添加回来
请参考 vue.config.js配置文件里引入的相关CDN。

#####build编译发布注意如下
> main.js   import ELEMENT from 'element-ui' 替换成 import ELEMENT from 'ELEMENT';
>/store/index.js 删除 Vue.use(Vuex);   //build编译发布需删除
>/router.js // Vue.use(VueRouter);    //build编译发布需删除


### 需要调用的服务器api接口
ajax请求接口目录
request/api.js 
```
/**
 * api 接口默认值及相关参数
 */
export default{
            // 查询多条
    		UserList:{
    			url:URL+"/admin/user/list",
    			headers:{
                },//设置请求头
    			data:{},//用于其他请求的数据
                params:{
                    access_token:true,
                    p:0,//页码
                    start_time:15655556666,//起始时间
                    last_time:15655576666,//截至时间
                    trashed:0,//是否被删除的
                }//用户get请求的数据
    		},
            AdminUser_List:{
               url:URL+"/admin/admin_user/list",
               params:{
                   access_token:true
               }//用户get请求的数据 
            },AdminUser_update:{
    			url:URL+"/admin/admin_user/update",
                method:'PUT',
                data:{
                    access_token:true
                }
            },
             // 添加后台管理人员
             AdminUser_Add:{
            	url:URL+"/admin/admin_user/add",
                method:'POST',
                data:{
                    user_id:'', 
                    rank:0,//权限
                    access_token:true
                }
            },// 删除单条
            AdminUser_Delete:{
    			url:URL+"/admin/admin_user/delete",
                method:'DELETE',
                data:{
                    user_id:'0',//要恢复的用户id
                    access_token:true
                }
    		},
            // 恢复一条
             AdminUser_Restore:{
                url:URL+"/admin/admin_user/restore",
                method:'PUT',
                data:{
                    user_id:0,//要恢复的用户id
                    access_token:true
                }  
            },
            // 全部功能
            AdminFunc_List:{
            	url:URL+"/admin/admin_func/list",
                params:{
                    access_token:true
                }//用户get请求的数据
            },
             // 用户的功能
            AdminRank_MyRankFunc:{
            	url:URL+"/admin/admin_rank/MyRankFunc",
                params:{
                    access_token:true
                }//用户get请求的数据
            },
            AdminUser_Myupdate:{
    			url:URL+"/admin/admin_user/MyUpdate",
                method:'PUT',
                data:{
                    access_token:true
                }
            },
            AdminRankFunc_Update:{
            	url:URL+"/admin/admin_rank_func/update",
                method:'PUT',
                data:{
                    access_token:true
                }
            },
            AdminRankFunc_Delete:{
              url:URL+"/admin/admin_rank_func/delete",
              method:'DELETE',
              data:{
                  access_token:true
              }  
            },
             AdminRankFunc_Add:{
              url:URL+"/admin/admin_rank_func/add",
              method:'POST',
              data:{
                  access_token:true
              }  
            },
             // 站主权限列表
            AdminRank_List:{
            	url:URL+"/admin/admin_rank/list",
                params:{
                    access_token:true
                }//用户get请求的数据
            },
            AdminRank_Add:{
            	url:URL+"/admin/admin_rank/add",
                method:'POST',
                params:{
                    access_token:true
                }//用户get请求的数据
            },// 删除单条
            AdminRank_Delete:{
    			url:URL+"/admin/admin_rank/delete",
                method:'DELETE',
                data:{
                    user_id:'0',//要恢复的用户id
                    access_token:true
                }
    		},
            // 恢复一条
             AdminRank_Restore:{
                url:URL+"/admin/admin_rank/restore",
                method:'PUT',
                data:{
                    user_id:0,//要恢复的用户id
                    access_token:true
                }  
            },
            // 修改
            AdminRank_Update:{
            	url:URL+"/admin/admin_rank/update",
                method:'PUT',
                data:{
                    access_token:true
                },
            },
            // 修改
            User_update:{
    			url:URL+"/admin/user/update",
                method:'PUT',
                data:{
                    access_token:true
                },
    			headers:{
                    // 'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'
                    // 'Content-Type':'multipart/form-data'
                    // "signature":JSON.stringify(configure.signature),
                    // "Content-Type":"application/x-www-form-urlencoded"
                }
    		},
            // 删除单条
            User_Delete:{
    			url:URL+"/admin/user/delete",
                method:'DELETE',
                data:{
                    id:'0',//要恢复的用户id
                    access_token:true
                }
    		},
            // 删除多条
            User_DeleteAll:{
                url:URL+"/admin/user/deleteAll",
                method:'DELETE',
                data:{
                    list:[],//要恢复的用户id
                    access_token:true
                } 
            },
            // 恢复一条
             User_Restore:{
                url:URL+"/admin/user/restore",
                method:'PUT',
                data:{
                    id:0,//要恢复的用户id
                    access_token:true
                }  
            },
             // 恢复多条
             User_RestoreAll:{
                url:URL+"/admin/user/restoreAll",
                method:'PUT',
                data:{
                    list:[],//要恢复的用户id
                    access_token:true
                }  
            },
             // 添加用户
             User_Add:{
    			url:URL+"/admin/user/add",
                method:'POST',
                data:{
                    username:'', 
                    password:'', 
                    rank:0,//权限
                    access_token:true
                }
    		},
             // 登陆
             Login:{
    			url:URL+"/admin/login/login",
                method:'GET',
                params:{
                    username:'', 
                    password:'',
                    captcha:'',//验证码
                    access_token:true
                }
    		},
             // 注册
            Register:{
    			url:URL+"/admin/login/register",
                method:'POST',
                data:{
                    username:'', 
                    password:'',
                    captcha:'',//验证码
                    access_token:true
                }
    		},Captcha:{
    			url:URL+"/admin/verify/captcha",
                method:'POST',
                data:{
                    id:'captcha',//验证场景
                    access_token:true
                }
    		}
}
```