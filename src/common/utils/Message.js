import {Notification} from 'element-ui';
function right(data){
    var data=data ||{};
    data.code=data.code || 400;
    data.message=data.message|| '失败';
    var type= 'warning';

     switch (data.code){
      	case 200:
         type= 'success';
      		break;
      	default:
      		break;
      }
      Notification({
          title: data.message,
          type: type
      });
}

export default{
    right
}