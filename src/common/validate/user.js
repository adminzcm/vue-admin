export default {
	error:'',
	check : function (data, rule){
				for(var i = 0; i < rule.length; i++){
					if (!rule[i].checkType){return true;}
					if (!rule[i].name) {return true;}
					if (!rule[i].error) {return true;}
					if (!data[rule[i].name]) {this.error = rule[i].error; return false;}
					switch (rule[i].checkType){
						case 'password':
						
							var PdZf=new RegExp("^[a-zA-Z0-9_,.?`@]+$","i").test(data[rule[i].name]);//判断是字母或数组或下横线,.@等字符
							var p = /[a-z]/i; 
							var PdZm = p.test(data[rule[i].name].substr(0,1));//判断首位是字母

							if (data[rule[i].name.length<8){
								this.error=rule[i].error || '请输入至少8位字符';
								return false;
							}else if(!PdZm){
								this.error=rule[i].error || '首字符只能是字母';
								return false;
							}else if(!PdZf) {
							  this.error=rule[i].error || '请使用(字母)+(数字)+常用英文字符组合';
							  return false;
							} 
						break;
					}
				}
	}
}