import Vue from 'vue';
// 引入echarts cdn
import echarts from 'echarts';
import store from './store/';
import App from './App.vue';
import router from './router';
import config from './config.js';
// import ELEMENT from 'ELEMENT';//发布环境用这个
// import ELEMENT from './element/index.js';
import ELEMENT from './element/index.js';

// import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ELEMENT);
Vue.prototype.$echarts = echarts;
Vue.prototype.$config = config;
// 阻止vue 在启动时生成生产提示
Vue.config.productionTip = false;
new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');
