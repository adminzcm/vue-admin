// import Time from '../common/utils/Time.js'
import Config from '../config.js'
// import Md5 from '../common/utils/Md5.js'
var systemAPI = Config.systemAPI;
var API=Config.api;
/**
 * api 接口默认值及相关参数
 */
export default {
    // 查询多条
    Admin_List: {
        url: systemAPI + "/admin/list",
        headers: {}, //设置请求头
        data: {}, //用于其他请求的数据
        params: {
            access_token: true,
            p: 0, //页码
            start_time: 15655556666, //起始时间
            last_time: 15655576666, //截至时间
            trashed: 0, //是否被删除的
        } //用户get请求的数据
    },
    // 恢复一条
    Admin_Restore: {
        url: systemAPI + "/admin/restore",
        method: 'PUT',
        data: {
            user_id: 0, //要恢复的用户id
            access_token: true
        }
    }, // 删除单条
    Admin_Delete: {
        url: systemAPI + "/admin/delete",
        method: 'DELETE',
        data: {
            user_id: '0', //要恢复的用户id
            access_token: true
        }
    }, // 添加后台管理人员
    Admin_Add: {
        url: systemAPI + "/admin/add",
        method: 'POST',
        data: {
            user_id: '',
            rank: 0, //权限
            access_token: true
        }
    },
    Admin_Update: {
        url: systemAPI + "/admin/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    SystemSafeGrade_List: {
        url: systemAPI + "/system_safe_grade/list",
        params: {
            access_token: true
        } //用户get请求的数据 
    },
    SystemSafe_Detail: {
        url: systemAPI + "/system_safe/detail",
        params: {
            access_token: true
        } //用户get请求的数据 
    },
    SystemSafe_Update: {
        url: systemAPI + "/system_safe/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    SystemSafeUrl_List: {
        url: systemAPI + "/system_safe_url/list",
        params: {
            access_token: true
        } //用户get请求的数据 
    },
    SystemSafeUrl_Update: {
        url: systemAPI + "/system_safe_url/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    SystemSafeUrl_Add: {
        url: systemAPI + "/system_safe_url/add",
        method: 'POST',
        data: {
            access_token: true
        }
    },
    // 删除一条
    SystemSafeUrl_Delete: {
        url: systemAPI + "/system_safe_url/delete",
        method: 'DELETE',
        data: {
            user_id: '0', //要恢复的用户id
            access_token: true
        }
    },
    // 恢复一条
    SystemSafeUrl_Restore: {
        url: systemAPI + "/system_safe_url/restore",
        method: 'PUT',
        data: {
            user_id: 0, //要恢复的用户id
            access_token: true
        }
    },
    AdminUser_List: {
        url: systemAPI + "/admin_user/list",
        params: {
            access_token: true
        } //用户get请求的数据 
    },
    AdminUser_update: {
        url: systemAPI + "/admin_user/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    // 添加后台管理人员
    AdminUser_Add: {
        url: systemAPI + "/admin_user/add",
        method: 'POST',
        data: {
            user_id: '',
            rank: 0, //权限
            access_token: true
        }
    }, // 删除单条
    AdminUser_Delete: {
        url: systemAPI + "/admin_user/delete",
        method: 'DELETE',
        data: {
            user_id: '0', //要恢复的用户id
            access_token: true
        }
    },
    // 恢复一条
    AdminUser_Restore: {
        url: systemAPI + "/admin_user/restore",
        method: 'PUT',
        data: {
            user_id: 0, //要恢复的用户id
            access_token: true
        }
    },
    AdminUser_Myupdate: {
        url: systemAPI + "/admin_user/MyUpdate",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    // 添加一个功能接口
    AdminFunc_Add: {
        url: systemAPI + "/admin_func/add",
        method: 'POST',
        data: {
            access_token: true
        }
    },
    // 添加一个功能接口
    AdminFunc_Update: {
        url: systemAPI + "/admin_func/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    // 全部功能名列表
    AdminFunc_List: {
        url: systemAPI + "/admin_func/list",
        params: {
            access_token: true
        } //用户get请求的数据
    },
    //全部功能详版 用于功能新增修改管理
    AdminFunc_DetailedList: {
        url: systemAPI + "/admin_func/detailedList",
        params: {
            access_token: true
        }
    },
    // 删除单条
    AdminFunc_Delete: {
        url: systemAPI + "/admin_func/delete",
        method: 'DELETE',
        data: {
            id: '0', //要恢复的用户id
            access_token: true
        }
    },
    // 恢复一条
    AdminFunc_Restore: {
        url: systemAPI + "/admin_func/restore",
        method: 'PUT',
        data: {
            id: 0, //要恢复的用户id
            access_token: true
        }
    },
    // 全部权限功能列表
    AdminRank_List: {
        url: systemAPI + "/admin_rank/list",
        params: {
            access_token: true
        } //用户get请求的数据
    },
    // 用户的功能
    AdminRank_MyRankFunc: {
        url: systemAPI + "/admin_rank/MyRankFunc",
        params: {
            access_token: true
        } //用户get请求的数据
    },
        // 删除单条
    AdminRank_Add: {
        url: systemAPI + "/admin_rank/add",
        method: 'POST',
        params: {
            access_token: true
        }
    }, // 删除单条
    AdminRank_Delete: {
        url: systemAPI + "/admin_rank/delete",
        method: 'DELETE',
        data: {
            user_id: '0', //要恢复的用户id
            access_token: true
        }
    },
    // 恢复一条
    AdminRank_Restore: {
        url: systemAPI + "/admin_rank/restore",
        method: 'PUT',
        data: {
            user_id: 0, //要恢复的用户id
            access_token: true
        }
    },
    // 修改
    AdminRank_Update: {
        url: systemAPI + "/admin_rank/update",
        method: 'PUT',
        data: {
            access_token: true
        },
    },
    
    AdminRankFunc_Update: {
        url: systemAPI + "/admin_rank_func/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    AdminRankFunc_Delete: {
        url: systemAPI + "/admin_rank_func/delete",
        method: 'DELETE',
        data: {
            access_token: true
        }
    },
    // 给用户添加新功能
    AdminRankFunc_Add: {
        url: systemAPI + "/admin_rank_func/add",
        method: 'POST',
        data: {
            access_token: true
        }
    },
    // 给组添加新功能
    AdminGroupFunc_Add: {
        url: systemAPI + "/admin_group_func/add",
        method: 'POST',
        data: {
            access_token: true
        }
    },
    AdminGroupFunc_Delete: {
        url: systemAPI + "/admin_group_func/delete",
        method: 'DELETE',
        data: {
            access_token: true
        }
    },
    AdminGroup_funcList: {
        url: systemAPI + "/admin_group/funcList",
        params: {
            access_token: true
        } //用户get请求的数据
    },
    AdminGroup_List: {
        url: systemAPI + "/admin_group/list",
        params: {
            access_token: true
        } //用户get请求的数据
    },
    AdminGroup_Add: {
        url: systemAPI + "/admin_group/add",
        method: 'POST',
        params: {
            access_token: true
        } //用户get请求的数据
    },
    // 修改
    AdminGroup_Update: {
        url: systemAPI + "/admin_group/update",
        method: 'PUT',
        data: {
            access_token: true
        },
    },
    // 删除单条
    AdminGroup_Delete: {
        url: systemAPI + "/admin_group/delete",
        method: 'DELETE',
        data: {
            user_id: '0', //要恢复的用户id
            access_token: true
        }
    }, // 恢复一条
    AdminGroup_Restore: {
        url: systemAPI + "/admin_group/restore",
        method: 'PUT',
        data: {
            user_id: 0, //要恢复的用户id
            access_token: true
        }
    },
    
 // 查询多条
    UserList: {
        url: systemAPI + "/user/list",
        headers: {}, //设置请求头
        data: {}, //用于其他请求的数据
        params: {
            access_token: true,
            p: 0, //页码
            start_time: 15655556666, //起始时间
            last_time: 15655576666, //截至时间
            trashed: 0, //是否被删除的
        } //用户get请求的数据
    },
    // 修改
    User_update: {
        url: systemAPI + "/user/update",
        method: 'PUT',
        data: {
            access_token: true
        },
        headers: {
            // 'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'
            // 'Content-Type':'multipart/form-data'
            // "signature":JSON.stringify(configure.signature),
            // "Content-Type":"application/x-www-form-urlencoded"
        }
    },
    // 删除单条
    User_Delete: {
        url: systemAPI + "/user/delete",
        method: 'DELETE',
        data: {
            id: '0', //要恢复的用户id
            access_token: true
        }
    },
    // 删除多条
    User_DeleteAll: {
        url: systemAPI + "/user/deleteAll",
        method: 'DELETE',
        data: {
            list: [], //要恢复的用户id
            access_token: true
        }
    },
    // 恢复一条
    User_Restore: {
        url: systemAPI + "/user/restore",
        method: 'PUT',
        data: {
            id: 0, //要恢复的用户id
            access_token: true
        }
    },
    // 恢复多条
    User_RestoreAll: {
        url: systemAPI + "/user/restoreAll",
        method: 'PUT',
        data: {
            list: [], //要恢复的用户id
            access_token: true
        }
    },
    // 添加用户
    User_Add: {
        url: systemAPI + "/user/add",
        method: 'POST',
        data: {
            username: '',
            password: '',
            rank: 0, //权限
            access_token: true
        }
    },
    // 登陆
    Login: {
        url:API + "/login/login",
        method: 'GET',
        params: {
            username: '',
            password: '',
            captcha: '', //验证码
            access_token: true
        }
    },
    resetLogin: {
        url: API + "/login/resetLogin",
        method: 'PUT',
        params: {
            access_token: true
        }
    },
    // 注册
    Register: {
        url: API + "/login/register",
        method: 'POST',
        data: {
            username: '',
            password: '',
            captcha: '', //验证码
            access_token: true
        }
    },
    Captcha: {
        url: API + "/verify/captcha",
        method: 'GET',
        params: {
            id: 'captcha', //验证场景
            access_token: true
        }
    }
}
