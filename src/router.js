import Vue from 'vue';
import VueRouter from 'vue-router'
Vue.use(VueRouter);//发布环境如果配置cdn 删除这个引入
var routerConfig ={
  routes: [{
                path: '/',
                name: '/',
                meta: { title: '后台首页' },
                component: () => import('./views/admin/index.vue'),
                redirect: '/chart',
                children:[
                {
                        path: '/chart',
                        name: 'chart',
                        meta: { title: '后台监控' },
                        component: () => import('./components/public/chart.vue')
                },{
                        path: '/system',
                        name: 'system',
                        meta: { title: '系统管理' },
                        component: () => import('./components/system/index.vue'),
                        children:[
                        {
                                 path: '/system_safeUrl',
                                 name: 'system_safeUrl',
                                 meta: { title: '鉴权拦截' },
                                 component: () => import('./components/system/safeUrl.vue')
                        },{
                                 path: '/system_safe',
                                 name: 'system_safe',
                                 meta: { title: '鉴权拦截' },
                                 component: () => import('./components/system/safe.vue')
                        },{
                                 path: '/system_func',
                                 name: 'system_func',
                                 meta: { title: '功能管理' },
                                 component: () => import('./components/system/func.vue')
                        },{
                                 path: '/system_user',
                                 name: 'system_user',
                                 meta: { title: '成员团队' },
                                 component: () => import('./components/system/user.vue')
                        },{
                                 path: '/system_group',
                                 name: 'system_group',
                                 meta: { title: '权限管理' },
                                 component: () => import('./components/system/group.vue')
                        }]
                },{
                        path: '/admin',
                        name: 'admin',
                        meta: { title: '后台管理' },
                        component: () => import('./components/admin/index.vue'),
                        children:[
                        {
                                 path: '/admin_user',
                                 name: 'admin_user',
                                 meta: { title: '成员团队' },
                                 component: () => import('./components/admin/user.vue')
                        },{
                                 path: '/admin_rank',
                                 name: 'admin_rank',
                                 meta: { title: '权限管理' },
                                 component: () => import('./components/admin/rank.vue')
                        },{
                                 path: '/admin_myFunc',
                                 name: 'admin_myFunc',
                                 meta: { title: '我的职权' },
                                 component: () => import('./components/admin/myFunc.vue')
                        }]
                },
                {
                        path: '/user',
                        name: 'user',
                        meta: { title: '用户列表' },
                        component: () => import('./components/user/index.vue'),
                        children:[
                        {
                                 path: '/user_list',
                                 name: 'user_list',
                                 meta: { title: '用户列表' },
                                 component: () => import('./components/user/list.vue')
                        }]
                }]
	},{
		path: '/login',
		name: 'login',
		meta: { title: '登陆' },
		component: () => import('./components/login/index.vue'),
        redirect: '/login_login',
        children:[
        {
                 path: '/login_login',
                 name: 'login_login',
                 meta: { title: '登陆' },
                 component: () => import('./components/login/login.vue')
        },
        {
                 path: '/login_register',
                 name: 'login_register',
                 meta: { title: '用户注册' },
                 component: () => import('./components/login/register.vue')
        }   
        ]
	}]
};
export default new VueRouter(routerConfig);
